#!/usr/bin/env python3

import gitlab
import os
import shutil
import sys

if not 'CI_JOB_TOKEN' in os.environ and not 'GITLAB_TOKEN' in os.environ:
  print("Set CI_JOB_TOKEN or GITLAB_TOKEN env var to your token")
  exit(1)

dry_run = False
if len(sys.argv) > 1 and sys.argv[1] == "--dry-run":
  dry_run = True

# job token authentication (to be used in CI)
# bear in mind the limitations of the API endpoints it supports:
# https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html
if 'CI_JOB_TOKEN' in os.environ:
  gl = gitlab.Gitlab('https://gitlab.freedesktop.org', job_token=os.environ['CI_JOB_TOKEN'])
elif 'GITLAB_TOKEN' in os.environ:
  gl = gitlab.Gitlab('https://gitlab.freedesktop.org', private_token=os.environ['GITLAB_TOKEN'])


# make an API request to create the gl.user object. This is not required but may be useful
# to validate your token authentication. Note that this will not work with job tokens.
# gl.auth()

# Enable "debug" mode. This can be useful when trying to determine what
# information is being sent back and forth to the GitLab server.
# Note: this will cause credentials and other potentially sensitive
# information to be printed to the terminal.

# gl.enable_debug()

project_names = [ 'gulkan', 'gxr', 'g3k', 'xrdesktop', 'libinputsynth', 'kwin-effect-xrdesktop', 'kdeplasma-applets-xrdesktop', 'gnome-shell', 'gnome-shell-extension-xrdesktop']

# branches that start with these prefixes are packaged
branches = [ "main" ] # , "legacy", "release-" ]

# but gnome-shell branches are special cases
gnome_shell_branches = [ "3.36.9-xrdesktop", "42.5-xrdesktop" ]

for project_name in project_names:
  print("Processing project " + project_name + "...")
  project = gl.projects.get('xrdesktop' + '/' + project_name)
  for branch in project.branches.list(get_all=True):
    branch_name = branch.get_id()

    package_branch = False

    if project_name == "gnome-shell":
      if branch_name in gnome_shell_branches:
        package_branch = True
    else:
      for branch in branches:
        if branch_name.startswith(branch):
          package_branch = True
      else:
        print("Ignoring branch " + branch_name)

    # TODO
    if not package_branch:
      continue

    if dry_run:
      print("Dry run, not fetching & extracting project ", project_name, " branch ", branch_name)
      continue

    print("Fetching artifacts for branch " + branch_name + "...")

    pipelines = project.pipelines.list(get_all=True)
    pipeline = pipelines[0]
    jobs = pipeline.jobs.list(get_all=True)
    #for job in jobs:
    #  job.pprint()


    print("Extracting artifacts...")

    zipfn = os.path.join(branch_name + "_" + project_name + "_" + "artifact.zip")
    with open(zipfn, "wb") as f:
      artifacts_stream = project.artifacts.download(ref_name=branch_name, job='reprepro:package', streamed=True, action=f.write)
    shutil.unpack_archive(zipfn, os.path.join("repos", branch_name, project_name))

    os.unlink(zipfn)
